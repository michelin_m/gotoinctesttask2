﻿using System;
using System.IO;

namespace testTask2
{
    class Program
    {
        static void Main(string[] args)
        {
            //optionally
            //--------------------------
            StreamReader sr = new StreamReader("pathToFile");
            string text = sr.ReadToEnd();
            sr.Close();
            //--------------------------
            //OR
            //--------------------------
            //string text = "Put your test text here";
            //--------------------------

            string[] texts = MostFrequentWords(text, out int[] c);

            int i = 0;
            foreach (var item in texts)
            {
                Console.WriteLine($"{i+1}. {item} - {c[i]}");i++;
            }
        }

        public static string[] MostFrequentWords(string text, out int[] c)
        {
            

            string[] texts = text.ToLower().Split(new char[] { ' ', ',', '.', '!', '?', '"' }, StringSplitOptions.RemoveEmptyEntries);
            string[] texts1 = new string[3];// For the most frequent words

            int[] wordCount = new int[texts.Length]; // Array for counts of each word

            c = new int[3]; // This is the out array for the indexes
            
            for (int i = 0; i < texts.Length; i++)
            {
                int count = 1;

                if (texts[i] != null) 
                {
                    for (int j = i + 1; j < texts.Length; j++)
                    {
                        if (texts[i] == texts[j])
                        {
                            count++;
                            texts[j] = null; // To delete words, that I've already counted
                        }
                    }
                }
                else
                    count = 0; // If count == 0, then this word has already been mentioned
                wordCount[i] = count;
            }


            // Loop that seeks for words that have max frequency
            for (int i = 0; i < 3; i++)
            {
                
                int maxId = 0;
                int max = 0;
                for (int j = 0; j < wordCount.Length; j++)
                {                    
                    if (wordCount[j] > max)
                    {
                        max = wordCount[j];
                        maxId = j;
                    }
                }
                if (max == 0)
                    return new string[0]; // Returns an empty array if it has less than 3 unique words

                texts1[i] = texts[maxId];
                c[i] = max; 
                wordCount[maxId] = 0;
            }
            
            return texts1;

        }
    }
}